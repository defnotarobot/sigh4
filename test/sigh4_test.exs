defmodule Sigh4Test do
  use ExUnit.Case
  doctest Sigh4

  test "greets the world" do
    assert Sigh4.hello() == :world
  end

  test "original properly stored in carrier" do
    %Sigh4.Carrier{ original: original} = Sigh4.Solve.random_test("This is a passage.")
    assert original == "THISISAPASSAGE"
  end

  test "determine character frequencies and perform chi squared" do
    chi2 = Sigh4.AnalStats.list_ave("This is an example passage.") |> Sigh4.AnalStats.chi2
    assert chi2 == 2.1908131598892857
  end

end
