defmodule Sigh4 do
  @moduledoc """
  Documentation for Sigh4.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Sigh4.hello()
      :world

  """
  def hello do
    :world
  end
end
