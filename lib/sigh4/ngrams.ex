defmodule Sigh4.Ngrams do
  alias Sigh4.{CharFreak}

  @moduledoc """
  Bigrams contains functions related to the analysis of bigrams in a text. 
  """

  def ngrams(passage,n) do
    passage
    |> CharFreak.filter_alpha
    |> Enum.chunk_every(n,1)
    |> Enum.filter(fn x -> length(x) == n end)
    |> Enum.map(fn x -> Enum.reduce(x, fn y,acc -> y <> acc end) end)
    |> Enum.reduce(Map.new, fn x,acc -> Map.update(acc, x, 1, &(&1+1)) end)
    |> Enum.sort_by(fn {_k,v} -> -v end)
  end

  def bigrams(passage) do
    passage
    |> ngrams(2)
  end

  def trigrams(passage) do
    passage
    |> ngrams(3)
  end

end
