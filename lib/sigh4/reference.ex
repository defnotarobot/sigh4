defmodule Sigh4.Reference do

  @alphabet ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

  @alphabet_by_frequency ["E","T","A","O","I","N","S","R","H","D","L","U","C","H","F","Y","W","G","P","B","V","K","X","Q","J","Z"]
  # From: http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
  @english_letter_frequencies %{
    "E" => 0.1202,
    "T" => 0.0910,
    "A" => 0.0812,
    "O" => 0.0768,
    "I" => 0.0731,
    "N" => 0.0695,
    "S" => 0.0628,
    "R" => 0.0602,
    "H" => 0.0592,
    "D" => 0.0432,
    "L" => 0.0398,
    "U" => 0.0288,
    "C" => 0.0271,
    "M" => 0.0261,
    "F" => 0.0230,
    "Y" => 0.0211,
    "W" => 0.0209,
    "G" => 0.0203,
    "P" => 0.0182,
    "B" => 0.0149,
    "V" => 0.0111,
    "K" => 0.0069,
    "X" => 0.0017,
    "Q" => 0.0011,
    "J" => 0.0010,
    "Z" => 0.0007,
  }

  @doc """
  This may be a bit unconventional, but I really need to be able to access the established English language  letter frequencies as constants. So that is what this function does. 

  ## Examples
  
      iex> Sigh4.Reference.freq_map
      %{
        "A" => 0.0812,
        "B" => 0.0149,
        "C" => 0.0271,
        "D" => 0.0432,
        "E" => 0.1202,
        "F" => 0.023,
        "G" => 0.0203,
        "H" => 0.0592,
        "I" => 0.0731,
        "J" => 0.001,
        "K" => 0.0069,
        "L" => 0.0398,
        "M" => 0.0261,
        "N" => 0.0695,
        "O" => 0.0768,
        "P" => 0.0182,
        "Q" => 0.0011,
        "R" => 0.0602,
        "S" => 0.0628,
        "T" => 0.091,
        "U" => 0.0288,
        "V" => 0.0111,
        "W" => 0.0209,
        "X" => 0.0017,
        "Y" => 0.0211,
        "Z" => 0.0007
      }


  """
  def freq_map do
    @english_letter_frequencies
  end

  def alphabet do
    @alphabet
  end

  def alpha_by_freq do
    @alphabet_by_frequency
  end
end
