defmodule Sigh4.Cipher do
  alias Sigh4.{CharFreak,Reference}

  @moduledoc """
  This module contains functions that relate to the generation of ciphers, and the translation of passage using those ciphers.
  """

  @doc """
  Generates a random cipher. 
  """
  def gen_cipher do
    alphabet = Reference.alphabet()
    equivalents = Enum.shuffle(alphabet)

    build = %{count: 26, cipher: []}
    map = builder(build,alphabet,equivalents)    
    map.cipher
  end

  @doc """
  Creates a decode map where the the frequency of letters in passage are paired directly with the expected letter frequencies in English.
  """
  def char_freq_predicted_cipher(passage) do
    CharFreak.freq_list(passage)
    |> Enum.map(fn x -> elem(x,0) end)
    |> Enum.zip(Reference.alpha_by_freq()) 
    |> Enum.into(%{})
  end

  defp builder(%{count: c} = map,_alpha,_eq) when c <= 0 do
    map
  end

  defp builder(map,alpha,eq) do
    builder(%{count: map.count - 1, cipher: map.cipher ++ [{Enum.at(alpha,map.count - 1),Enum.at(eq,map.count - 1)}]},alpha,eq)
  end


  def translate(passage,cipher_map) when is_binary(passage) do
    passage
    |> CharFreak.filter_alpha
    |> Enum.map(fn a -> Map.fetch!(cipher_map, String.to_atom(a)) end)
    |> List.to_string
  end

  def translate(passage,cipher_map) when is_list(passage) do  
    passage
    |> List.to_string
    |> translate(cipher_map)
  end

  def cipher_encode_map(list) do
    list
    |> Enum.map(fn x -> Map.put(%{}, String.to_atom(elem(x,0)), elem(x,1)) end)
    |> Enum.reduce(fn x,acc ->  Map.merge(acc,x) end)
  end

  def cipher_decode_map(list) do
    list
    |> Enum.map(fn x -> Map.put(%{}, String.to_atom(elem(x,1)), elem(x,0)) end)
    |> Enum.reduce(fn x,acc ->  Map.merge(acc,x) end)
  end

end
