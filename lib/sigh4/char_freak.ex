defmodule Sigh4.CharFreak do

  @moduledoc """
  CharFreak contains functions that directly relate to character analysis.
  """


  def filter_alpha(passage) do
    passage
    |> String.upcase
    |> String.graphemes
    |> Enum.filter(fn c -> c =~ ~r/[A-Z]/ end)
  end


  def total_alpha(passage) do
    passage
    |> filter_alpha
    |> length
  end

  @doc """
  Returns a list of two value tuples, one for each character present in the passage passed to it as a string.
  The tuples each hold two values: the character described, and its frequency as an integer.

  ## Examples

      iex>  passage = "Sphinx of black quartz, judge my vow."
      iex>  Sigh4.CharFreak.freq_list(passage)
      [
        {"A", 2},
        {"O", 2},
        {"U", 2},
        {"B", 1},
        {"C", 1},
        {"D", 1},
        {"E", 1},
        {"F", 1},
        {"G", 1},
        {"H", 1},
        {"I", 1},
        {"J", 1},
        {"K", 1},
        {"L", 1},
        {"M", 1},
        {"N", 1},
        {"P", 1},
        {"Q", 1},
        {"R", 1},
        {"S", 1},
        {"T", 1},
        {"V", 1},
        {"W", 1},
        {"X", 1},
        {"Y", 1},
        {"Z", 1}
      ]

  """
  def freq_list(passage) do
    passage
    |> filter_alpha
    |> Enum.reduce(Map.new, fn c,acc -> Map.update(acc, c, 1, &(&1+1)) end)
    |> Enum.sort_by(fn {_k,v} -> -v end)
  end

  @doc """
  Returns the output of 'freq_list' in the form of a map.

  ## Examples

      iex> passage = "Sphinx of black quartz, judge my vow."
      iex> map_freq(passage)
      %{
        "A" => 2,
        "B" => 1,
        "C" => 1,
        "D" => 1,
        "E" => 1,
        "F" => 1,
        "G" => 1,
        "H" => 1,
        "I" => 1,
        "J" => 1,
        "K" => 1,
        "L" => 1,
        "M" => 1,
        "N" => 1,
        "O" => 2,
        "P" => 1,
        "Q" => 1,
        "R" => 1,
        "S" => 1,
        "T" => 1,
        "U" => 2,
        "V" => 1,
        "W" => 1,
        "X" => 1,
        "Y" => 1,
        "Z" => 1
      }

  """
  def map_freq(passage) do
    passage
    |> freq_list
    |> Enum.into(%{})
  end

  def map_freq_ratio(passage) do
    passage
    |> freq_list
    |> Enum.map(fn x -> {elem(x,0),elem(x,1)/total_alpha(passage)} end)
    |> Enum.into(%{})
  end

end
