defmodule Sigh4.AnalStats do
  alias Sigh4.{CharFreak,Reference}

  def single_chi(count_actual,count_expected) do
    (count_actual - count_expected) * (count_actual - count_expected) / count_expected
  end

  def chi2(values) do
    # Expects values to be a list of tuples: [{C1,E1},{C2,E2},...]
    values
    |> Enum.map(fn x -> single_chi(elem(x,0),elem(x,1)) end)
    |> Enum.sum
  end

  @doc """
  Prepares {actual,expected} tuple for a given character.
  """
  def char_ave(char,passage) do
    freq_map = CharFreak.map_freq_ratio(passage)
    expect_map = Reference.freq_map
    cap_char = String.capitalize(char)

    ret = Map.fetch(freq_map,cap_char)
    case ret do
      {:ok,value} ->
        {value,Map.fetch!(expect_map,cap_char)}
      _ ->
        {0.0,Map.fetch!(expect_map,cap_char)}
    end
  end

  def list_ave(passage) do
    Enum.map(Reference.alphabet, fn a -> char_ave(a,passage) end)
  end

  def map_list_ave(passage) do
    Map.new(Reference.alphabet, fn a -> {a,char_ave(a,passage)} end)
  end

  def run_chi2(passage) do
    passage
    |> list_ave() 
    |> chi2()
  end

  def greatest_map_value(map) do
    map
    |> Map.values() 
    |> Enum.sort() 
    |> Enum.reverse() 
    |> Enum.at(0)
  end

  def key_by_value(value,map) do
    map
    |> Enum.find(fn {key, val} -> val == value end)
    |> elem(0)
  end


end
