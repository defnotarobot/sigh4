defmodule Sigh4.Solve do
  alias Sigh4.{AnalStats, Carrier, CharFreak, Cipher}

  def random_test(passage) do
    %Carrier{ original: CharFreak.filter_alpha(passage) |> List.to_string() } 
    |> carrier_gen_cipher()
    |> carrier_encode_map()
    |> carrier_decode_map()
    |> carrier_translated()
    |> carrier_original_chi2_total()
    |> carrier_translated_chi2_total()
  end

  def random_chi_test(passage) do
    carrier = random_test(passage)
    IO.puts "Original: " <> carrier.original
    IO.puts "Chi2: " <> Float.to_string(carrier.original_chi2_total)
    IO.puts "-------"
    IO.puts "Translated: " <> carrier.translated
    IO.puts "Chi2: " <> Float.to_string(carrier.translated_chi2_total)
  end

  def chi2_distribution(passage) do
    List.duplicate(:foo,5000)
    |> Stream.map(fn x ->  random_test(passage) end)
    |> Stream.map(fn x -> Float.to_string(x.translated_chi2_total) <> "\n" end)
    |> Stream.into(File.stream!(Integer.to_string(System.os_time), [:write, :utf8]))
    |> Stream.run
  end

  defp carrier_gen_cipher(carrier) do
    %Carrier{ carrier | cipher: Cipher.gen_cipher() }
  end

  defp carrier_encode_map(%Carrier{ cipher: cipher } = carrier) do
    %Carrier{ carrier | encode_map: Cipher.cipher_encode_map(cipher) }
  end

  defp carrier_decode_map(%Carrier{ cipher: cipher } = carrier) do
    %Carrier{ carrier | decode_map: Cipher.cipher_decode_map(cipher) }
  end

  defp carrier_translated(%Carrier{ original: original, encode_map: encode } = carrier) do
    %Carrier{ carrier | translated: Cipher.translate(original, encode) }
  end

  defp carrier_original_chi2_total(%Carrier{ original: original } = carrier) do
    %Carrier{ carrier | original_chi2_total: AnalStats.run_chi2(original) }
  end

  defp carrier_translated_chi2_total(%Carrier{ translated: translated } = carrier) do
    %Carrier{ carrier | translated_chi2_total: AnalStats.run_chi2(translated) }
  end

end
