defmodule Sigh4.Carrier do
  defstruct  original: nil, cipher: nil, encode_map: nil, decode_map: nil, translated: nil, original_chi2_total: nil, translated_chi2_total: nil
end
