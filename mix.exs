defmodule Sigh4.MixProject do
  use Mix.Project

  def project do
    [
      app: :sigh4,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Sigh4",
      source_url: "https://gitlab.com/defnotarobot/sigh4",
      homepage_url: "",
      docs: [
        main: "Sigh4", # The main page in the docs
        logo: "",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.19", only: :dev, runtime: false}
    ]
  end
end
